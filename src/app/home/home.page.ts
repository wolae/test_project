import { Component, OnInit } from '@angular/core';
import { LoginCredentials } from '../core/models/login-credentials.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  logged = false;
  logingData: LoginCredentials;
  pageTitle: string;
  constructor() {}
  ngOnInit() {
    this.initValues();
  }
  private initValues() {
    this.pageTitle = 'Prueba técnica';
  }
  public loginCredentials(data: LoginCredentials) {
    this.logingData = data;
    this.logged = true;
    console.log('LoginData', this.logingData);
  }
}
