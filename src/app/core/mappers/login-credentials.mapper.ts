import { LoginCredentials } from '../models/login-credentials.model';

export class LoginCredentialsMapper {
  public static mapTo(
    email: string,
    password: string,
    rememberMe: boolean
  ): LoginCredentials {
    const newLoginCredential: LoginCredentials = new LoginCredentials();
    newLoginCredential.email = email;
    newLoginCredential.password = password;
    newLoginCredential.rememberMe = rememberMe;
    return newLoginCredential;
  }
}
