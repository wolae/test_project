export class LoginCredentials {
  email: string;
  password: string;
  rememberMe: boolean;
}
