import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoComponent } from './components/logo/logo.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { TitleComponent } from './components/title/title.component';

@NgModule({
  declarations: [
    LogoComponent,
    LoginFormComponent,
    ConfirmComponent,
    TitleComponent,
  ],
  imports: [IonicModule, CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    LogoComponent,
    LoginFormComponent,
    ConfirmComponent,
    TitleComponent,
  ],
})
export class SharedModule {}
