import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LoginCredentialsMapper } from 'src/app/core/mappers/login-credentials.mapper';
import { LoginCredentials } from 'src/app/core/models/login-credentials.model';

import { LoginFormComponent } from './login-form.component';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [LoginFormComponent],
        imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
      }).compileComponents();

      fixture = TestBed.createComponent(LoginFormComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('Should show validation errors ', () => {
    it('On submit with empty email field, should show email required message', () => {
      component.onSubmit();
      fixture.detectChanges();
      const rendered = fixture.nativeElement;
      const emailRequiredMessage = rendered.querySelector(
        '.qa-formLogin-emailRequired'
      ).innerHTML;
      expect(emailRequiredMessage).toMatch('Email es requerido.');
    });
    it('On submit with empty password field, should show password required message', () => {
      component.onSubmit();
      fixture.detectChanges();
      const rendered = fixture.nativeElement;
      const passRequiredMessage = rendered.querySelector(
        '.qa-formLogin-passRequired'
      ).innerHTML;
      expect(passRequiredMessage).toMatch('Contraseña es requerida.');
    });
    it('On submit with invalid email, should show invalid email message', () => {
      const badEmail = 'my.email.com';
      component.loginForm.controls['email'].setValue(badEmail);
      component.onSubmit();
      fixture.detectChanges();
      const rendered = fixture.nativeElement;
      const passRequiredMessage = rendered.querySelector(
        '.qa-formLogin-emailPattern'
      ).innerHTML;
      expect(passRequiredMessage).toMatch('Por favor ingresa un email valido.');
    });
    it('On submit with invalid password, should show invalid pass message', () => {
      const badPass = '1234';
      component.loginForm.controls['password'].setValue(badPass);
      component.onSubmit();
      fixture.detectChanges();
      const rendered = fixture.nativeElement;
      const passRequiredMessage = rendered.querySelector(
        '.qa-formLogin-passMinLength'
      ).innerHTML;
      expect(passRequiredMessage).toMatch(
        'Debe tener un minimo de 5 caracteres.'
      );
    });
  });
  it('Should emit form values on submit', () => {
    spyOn(component.loginCredentials, 'emit');
    const niceEmail = 'myname@domain.com';
    const nicePassword = '123456';
    component.loginForm.controls['email'].setValue(niceEmail);
    component.loginForm.controls['password'].setValue(nicePassword);
    component.loginForm.controls['rememberMe'].setValue(true);
    component.onSubmit();
    fixture.detectChanges();
    const loginFormData: LoginCredentials = LoginCredentialsMapper.mapTo(
      niceEmail,
      nicePassword,
      true
    );
    expect(component.loginCredentials.emit).toHaveBeenCalledWith(loginFormData);
  });
  describe('Should show user pass without masking', () => {
    it('Without a password should not appear eye icon', () => {
      component.loginForm.controls['password'].setValue('');
      fixture.detectChanges();
      const rendered = fixture.nativeElement;
      const eyeOffIcon = rendered.querySelector('.qa-formLogin-pass-eye-off')
        .classList;

      expect(eyeOffIcon).toContain('hidden');
    });
    it('Given a password should appear eye off icon', () => {
      const pass = 'abc';
      component.loginForm.controls['password'].setValue(pass);
      fixture.detectChanges();
      const rendered = fixture.nativeElement;
      const eyeOffIcon = rendered.querySelector('.qa-formLogin-pass-eye-off')
        .classList;
      expect(eyeOffIcon).not.toContain('hidden');
    });
    describe('When user click on eye-off icon', () => {
      it('Input password type should be text', async () => {
        component.setTypeToText();
        fixture.detectChanges();
        const rendered = fixture.nativeElement;
        const passwordInput = rendered.querySelector('.qa-formLogin-passInput')
          .type;

        expect(passwordInput).toEqual('text');
      });
      it('Should appear a progress bar', async () => {
        component.showLoader = true;
        fixture.detectChanges();
        const rendered = fixture.nativeElement;
        const progressBar = await rendered.querySelector(
          '.qa-formLogin-progressBar'
        );
        expect(progressBar).toBeTruthy();
      });
    });
  });
});
