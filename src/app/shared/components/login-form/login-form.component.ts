import {
  Component,
  EventEmitter,
  HostListener,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { LoginCredentialsMapper } from 'src/app/core/mappers/login-credentials.mapper';
import { LoginCredentials } from 'src/app/core/models/login-credentials.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Output()
  loginCredentials: EventEmitter<LoginCredentials> = new EventEmitter<LoginCredentials>();
  loginForm: FormGroup;
  type: string = 'password';
  showLoader: boolean;
  progressBarValue: number;
  isSubmitted = false;
  remember = false;
  progressTimeOut = null;
  mobileView = false;
  screenHeight: number;
  screenWidth: number;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.screenWidth < 960
      ? (this.mobileView = true)
      : (this.mobileView = false);
  }

  constructor(private readonly fb: FormBuilder) {
    this.getScreenSize();
  }

  ngOnInit() {
    this.initForm();
  }
  private initForm() {
    this.loginForm = this.fb.group({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
      ]),
      password: new FormControl('', [
        Validators.minLength(5),
        Validators.required,
      ]),
      rememberMe: new FormControl(this.remember),
    });
  }
  private showProgressBar() {
    this.showLoader = true;
  }
  private hideProgressBar() {
    this.showLoader = false;
  }
  setTypeToText() {
    this.type = 'text';
  }
  setTypeToPassword() {
    this.type = 'password';
  }
  private setPercentBar(index) {
    if (!this.showLoader) {
      setTimeout(() => {
        this.showProgressBar();
        this.progressBarValue = index / 100;
        if (this.progressBarValue === 1) {
          this.setTypeToPassword();
          this.hideProgressBar();
          this.progressBarValue = null;
        }
      }, 30 * index);
    }
  }

  get errorControl() {
    return this.loginForm.controls;
  }

  public runProgress() {
    this.setTypeToText();
    for (let index = 0; index <= 100; index++) {
      this.setPercentBar(+index);
    }
  }
  public rememberMe(event: any) {
    this.remember = event.detail.checked;
  }
  public onSubmit() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      return false;
    } else {
      const email = this.loginForm.controls['email'].value;
      const password = this.loginForm.controls['password'].value;
      const rememberMe = this.loginForm.controls['rememberMe'].value;
      this.loginCredentials.emit(
        LoginCredentialsMapper.mapTo(email, password, rememberMe)
      );
    }
  }
}
