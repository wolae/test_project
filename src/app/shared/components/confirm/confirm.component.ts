import { Component, Input, OnInit } from '@angular/core';
import { LoginCredentials } from 'src/app/core/models/login-credentials.model';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit {
  @Input() loginData: LoginCredentials;
  constructor() {}

  ngOnInit() {}
}
