describe("Home page", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  describe("Should see the login form", () => {
    describe("Should show errors when submit with empty fields", () => {
      it("Should be a required email error message", () => {
        cy.get(".qa-formLogin-submitBtn").click();

        cy.get(".qa-formLogin-emailRequired");
      });
      it("Should be a required password error message", () => {
        cy.get(".qa-formLogin-submitBtn").click();

        cy.get(".qa-formLogin-passRequired");
      });
    });
    describe("Should show errors when validators fails", () => {
      it("Should show an email error message with invalid email", () => {
        const badMail = "my.mail.com";
        cy.get(".qa-formLogin-submitBtn").click();
        cy.get(".qa-formLogin-emailInput").type(badMail);
        cy.get(".qa-formLogin-emailPattern");
      });
      it("Should show password error message with invalid length", () => {
        cy.get(".qa-formLogin-submitBtn").click();
        const badPassword = "123";
        cy.get(".qa-formLogin-passInput").type(badPassword);
        cy.get(".qa-formLogin-passMinLength");
      });
    });

    it("Should login with my credentials and see success confirmation", () => {
      const niceMail = "my@mail.com";
      const nicePassword = "123456";
      cy.get(".qa-formLogin-emailInput").type(niceMail);
      cy.get(".qa-formLogin-passInput").type(nicePassword);
      cy.get(".qa-formLogin-rememberMe").click();
      cy.get(".qa-formLogin-submitBtn").click();
      cy.get(".qa-confirmComponent-successIcon");
    });
  });
});
