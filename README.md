# test_project

## Instalación proyecto

npm install -g @ionic/cli

npm install

## Levantar el proyecto

ionic serve

## Lanzar test unitarios

npm test

## Lanzar test e2e

npx cypress open

Se levantará una ventana de cypress donde podremos encontrar login.spec.js.

Para lanzar el test se ha de clickar en el elemento
